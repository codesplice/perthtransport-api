{-# LANGUAGE DeriveGeneric #-}
module Types where 

import Data.Aeson 
import Data.Time
import GHC.Generics

data Station = Station { id :: String, name :: String, lat :: Double, long :: Double } deriving (Generic, Show)

instance ToJSON Station

data Departure = Departure { time :: ZonedTime, destination :: String, pattern :: String, status :: String } deriving (Generic, Show)

instance ToJSON Departure