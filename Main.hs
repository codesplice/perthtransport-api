{-# LANGUAGE OverloadedStrings #-}
import Web.Scotty
import Control.Monad (liftM)
import Control.Monad.Trans (liftIO)
import Network.HTTP.Types
import System.Posix.Env
import Control.Exception
import Types
import Transperth

main = do
  port <- liftM read $ getEnvDefault "PORT" "3000" 
  scotty port $ do
    get "/train/" $ do
      list <- liftIO stations
      json list
    
    get "/train/near" $ do
      y <- param "lat"
      x <-param "long"
      list <- liftIO $ stationsNear y x
      json list
    
    get "/train/:station" $ do
      stationId <- param "station"
      station <- liftIO $ station stationId
      case station of
        Just s -> do
          times <- liftIO $ getTrainTimes $ name s
          json times
        Nothing ->
          Web.Scotty.status status404
          
    get "/error" $ do
      list <- liftIO $ throwIO $ MalformedTimeTableException
      Web.Scotty.status status200