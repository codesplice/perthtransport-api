{-#LANGUAGE DeriveDataTypeable#-}

module Transperth where

import Network.HTTP
import Text.HTML.TagSoup
import Types
import Data.Char
import Data.String.Utils
import Data.Maybe
import Data.Typeable
import Control.Monad
import Control.Exception
import Geo.Computations
import Data.Time.LocalTime
import Data.Time.Format
import Data.Time.Clock
import Data.Time.Calendar

data HtmlParseException = MissingEndTagException { tagName :: String }
                          | MalformedTimeTableException
    deriving (Show, Typeable)
instance Exception HtmlParseException

waTimeZone = TimeZone (8*60) False "AWST"

parseTimeOfDay :: String -> TimeOfDay
parseTimeOfDay = parseTimeOrError True defaultTimeLocale "%H:%M"

parseZonedTime :: TimeZone -> LocalTime -> String -> ZonedTime
parseZonedTime z d t
      | scheduledTime >= currentTime = ZonedTime (LocalTime (localDay d) scheduledTime) z
      | scheduledTime < currentTime = ZonedTime (LocalTime (addDays 1 $ localDay d) scheduledTime) z
   where currentTime = localTimeOfDay d
         scheduledTime = parseTimeOfDay t
  
parseWATime = parseZonedTime waTimeZone

openURL x = getResponseBody =<< simpleHTTP (getRequest x)
  
tagsInside :: String -> [Tag String] -> [[Tag String]]
tagsInside x tags = map f $ partitions (isTagOpenName x) tags
                 where f [] = throw $ MissingEndTagException x
                       f xs = tail $ takeWhile (not . isTagCloseName x) xs

tables :: [Tag String] -> [[Tag String]] 
tables = tagsInside "table"

rows :: [Tag String] -> [[Tag String]] 
rows = tagsInside "tr"

cells :: [Tag String] -> [[Tag String]] 
cells = tagsInside "td"

cellAtColumn :: Int -> [[Tag String]] -> [Tag String]
cellAtColumn n cs = cs !! n

textFromCell :: [Tag String] -> String
textFromCell = unwords . words . innerText 

patternFromCell :: [Tag String] -> String
patternFromCell = textFromCell . head . (tagsInside "strong")

destFromCell :: [Tag String] -> String
destFromCell = (replace "To " "") . textFromCell

getTrainTimes :: String -> IO [Departure]
getTrainTimes x = do tags <- fmap parseTags $ openURL $ "http://www.transperth.wa.gov.au/Timetables/Live-Train-Times?stationname=" ++ (urlEncode x)
                     let table = head $ tables tags -- first table in page
                     let rowArray = reverse . tail . reverse . tail $ rows table -- strip first & last rows
                     times <- sequence $ map (f . cells) rowArray -- convert each row into a Departure
                     return times
                  where f (a:b:c:d:cs) = do
                            utcTime <- getCurrentTime
                            let tenMinutesAgo = addUTCTime (-10 * 60) utcTime
                            let departureTime = parseWATime (utcToLocalTime waTimeZone tenMinutesAgo) (textFromCell a)
                            return $ Departure departureTime (destFromCell b) (patternFromCell c) (textFromCell d)
                        f cs = throwIO MalformedTimeTableException 

slugify :: String -> String
slugify = (map toLower). (replace " " "_") . (replace " Stn" "")
 
stations :: IO [Station]
stations = do file <- readFile "./station-locations.txt"
              let lines = split "\n" file
              let values = map (split ", ") lines
              return $ map f values
            where f l = Station (slugify $ l !! 0) (l !! 0) (read $ l !! 1) (read $ l !! 2)
            
station :: String -> IO (Maybe Station) 
station x = do ss <- stations
               let s = listToMaybe $ filter (\s -> Types.id s == x) ss
               return s
               
stationsNear :: Double -> Double -> IO [Station]
stationsNear y x = do ss <-  stations 
                      return $ filter (\s -> dist s < 1800.0) ss
                      where dist s = distance (pt (lat s) (long s) Nothing Nothing) (pt y x Nothing Nothing)