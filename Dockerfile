FROM haskell:7.10.2

RUN cabal update

# Add .cabal file
ADD ./perthtransport.cabal /opt/app/perthtransport.cabal

# Install gps-1.2 from source 
#ADD gps /opt/app/gps
#RUN cd /opt/app/gps && cabal install

# Docker will cache this command as a layer, freeing us up to
# modify source code without re-installing dependencies
RUN cd /opt/app && cabal install --only-dependencies -j4

# Add and Install Application Code
ADD . /opt/app
RUN cd /opt/app && cabal install

# Add installed cabal executables to PATH
ENV PATH /root/.cabal/bin:$PATH

EXPOSE 3000

# Default Command for Container
WORKDIR /opt/app
CMD ["perthtransport"]